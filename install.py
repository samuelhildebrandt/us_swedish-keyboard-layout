#!/usr/bin/python
import fileinput
import linecache
import os
import xml.etree.ElementTree as eT
import subprocess

def append_to_us_symbols(file):
    with open('/usr/share/X11/xkb/symbols/us', 'a') as f:
        f.write(os.linesep)
        f.write(open(file).read())
        f.write(os.linesep)


def add_to_variant_lst(file):
    for line in fileinput.input('/usr/share/X11/xkb/rules/evdev.lst', inplace=True):
        print(line, end='')
        if line.startswith('! variant'):
            print(open(file).read())


def add_to_variant_xml(file):
    doctype = get_doctype()

    variant = eT.parse(file).getroot()
    variant.tail = os.linesep + ' ' * 8

    evdev = eT.parse('/usr/share/X11/xkb/rules/evdev.xml')
    for layout in evdev.getroot().find('layoutList'):
        if layout.find('configItem').find('name').text == 'us':
            layout.find('variantList').insert(0, variant)
    evdev.write('/usr/share/X11/xkb/rules/evdev.xml', encoding='UTF-8', xml_declaration=True)

    add_back_doctype(doctype)


def get_doctype():
    return linecache.getline('/usr/share/X11/xkb/rules/evdev.xml', 2)


def add_back_doctype(doctype):
    if doctype.startswith('<!DOCTYPE'):
        with open('/usr/share/X11/xkb/rules/evdev.xml', 'r') as f:
            lines = f.readlines()
        lines.insert(1, doctype)
        with open('/usr/share/X11/xkb/rules/evdev.xml', 'w') as f:
            f.writelines(lines)


def add_variant(lst_file, xml_file):
    add_to_variant_lst(lst_file)
    add_to_variant_xml(xml_file)


append_to_us_symbols('us_swedish_xkb_symbols')
add_variant('us_swedish_variant-evdev.lst', 'us_swedish_variant-evdev.xml')

subprocess.call(['dconf', 'write', '/org/gnome/desktop/input-sources/xkb-options', "['caps:swapescape']"])

